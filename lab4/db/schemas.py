from typing import List, Optional
from pydantic import BaseModel

class StudentBase(BaseModel):
    npm: str
    name: str
    address: str

class StudentCreate(StudentBase):
    pass

class Student(StudentBase):
    id: int

    class Config:
        orm_mode = True

class File(BaseModel):
    id: int
    name: str
    binary: bytes

    class Config:
        orm_mode = True