from sqlalchemy import Column, Integer, LargeBinary, String
from .database import Base

class Student(Base):
    __tablename__ = "students"

    id = Column(Integer, primary_key=True, index=True)
    npm = Column(String, unique=True, index=True)
    name = Column(String)
    address = Column(String)

class File(Base):
    __tablename__ = "files"

    id = Column(Integer, primary_key=True, index=True)
    name = Column(String)
    binary = Column(LargeBinary)