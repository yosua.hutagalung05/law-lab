from sqlalchemy.orm import Session
from . import models, schemas

def get_student(db: Session, student_id: int):
    return db.query(models.Student).filter(models.Student.id == student_id).first()

def get_student_by_npm(db: Session, student_npm: str):
    return db.query(models.Student).filter(models.Student.npm == student_npm).first()

def get_students(db: Session, skip: int = 0, limit: int = 100):
    return db.query(models.Student).offset(skip).limit(limit).all()

def create_student(db: Session, student: schemas.StudentCreate):
    db_student = models.Student(npm=student.npm, name=student.name, address=student.address)
    db.add(db_student)
    db.commit()
    db.refresh(db_student)
    return db_student

def update_student(db: Session, student: schemas.StudentCreate, db_student = models.Student):
    db_student.npm = student.npm
    db_student.name = student.name
    db_student.address = student.address
    db.commit()
    db.refresh(db_student)
    return db_student

def delete_student(db: Session, student: schemas.StudentCreate):
    db.delete(student)
    db.commit()

def create_file(db: Session, file: schemas.File):
    db_file = models.File(name=file.name, binary=file.binary)
    db.add(db_file)
    db.commit()
    db.refresh(db_file)
    return db_file.name