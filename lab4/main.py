from typing import List
from fastapi import Depends, FastAPI, HTTPException, UploadFile
from sqlalchemy.orm import Session
from db import crud, models, schemas
from db.database import SessionLocal, engine

models.Base.metadata.create_all(bind=engine)

app = FastAPI()

# Dependency
def get_db():
    db = SessionLocal()
    try:
        yield db
    finally:
        db.close()

@app.get("/students", response_model=List[schemas.Student])
def read_users(skip: int = 0, limit: int = 100, db: Session = Depends(get_db)):
    students = crud.get_students(db, skip=skip, limit=limit)
    return students

@app.get("/students/{student_npm}", response_model=schemas.Student)
def read_user_by_npm(student_npm: str, db: Session = Depends(get_db)):
    db_student = crud.get_student_by_npm(db, student_npm=student_npm)

    # Raises error if student is not found
    if db_student is None:
        raise HTTPException(status_code=404, detail="Student not found.")

    return db_student

@app.post("/students", response_model=schemas.Student)
def create_student(student: schemas.StudentCreate, db: Session = Depends(get_db)):
    db_student = crud.get_student_by_npm(db, student_npm=student.npm)

    # Raises error if student with the same npm already exists
    if db_student:
        raise HTTPException(status_code=400, detail="Student with the same NPM already exists.")

    return crud.create_student(db, student=student)

@app.put("/students/{student_npm}", response_model=schemas.Student)
def update_student(student: schemas.StudentCreate, student_npm: str, db: Session = Depends(get_db)):
    db_student = crud.get_student_by_npm(db, student_npm=student_npm)

    if db_student is None:
        raise HTTPException(status_code=404, detail="Student not found.")
    
    return crud.update_student(db, student=student, db_student=db_student)

@app.delete("/students/{student_npm}")
def delete_student(student_npm: str, db: Session = Depends(get_db)):
    db_student = crud.get_student_by_npm(db, student_npm=student_npm)

    if db_student is None:
        raise HTTPException(status_code=404, detail="Student not found.")
    
    crud.delete_student(db, student=db_student)
    
    return {
        "message": "Student removed",
    }

@app.post("/files")
async def upload_file(file: UploadFile, db: Session = Depends(get_db)):
    contents = file.file.read()
    db_file = models.File(name=file.filename, binary=contents)
    db.add(db_file)
    db.commit()
    db.refresh(db_file)
    
    return {
        "name": db_file.name
    }