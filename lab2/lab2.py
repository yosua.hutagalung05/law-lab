from fastapi import FastAPI, Form, Request

app = FastAPI()

@app.get("/status_kekayaan")
async def status_kekayaan(nominal: int):
    if nominal < 200000 and nominal >= 0:
        status_kekayaan = "Oke"
    elif nominal >= 200000:
        status_kekayaan = "Lumayan"
    else:
        status_kekayaan = "Kebanyakan hutang"
    return {"status_kekayaan": status_kekayaan}

@app.post("/sapaan_satu")
async def sapaan_satu(nama_depan: str = Form(...), nama_belakang: str=Form(...)):
    return {"sapaan_satu": "Selamat pagi, " + nama_depan + " " + nama_belakang + "!"}

@app.post("/sapaan_dua")
async def sapaan_dua(request: Request):
    request_json = await request.json()
    nama_depan = request_json["nama_depan"]
    nama_belakang = request_json["nama_belakang"]
    return {"sapaan_dua": "Tidur terus kerjaan kau, " + nama_depan + " " + nama_belakang + "!"}